db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);
// [Section] MongoDB Aggregation

/*

MongoDB aggregation
- used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
-compared doing CRUD operations, limited, aggregation gives us access to manipulate, filtering and compute results
providing us with information to make necessary development decisions w/o having to create a frontend application
*/


// Using the AGGREGATE Method
/*
    $match is used to  pass the documents that meet the specified condition/s the next pipleline stage/aggregation process

    Syntax:

        {$match: {field:value}}
*/

db.fruits.aggregate
([
    {
        $match: {onSale:true}
    }
]);
// $Group
db.fruits.aggregate([
    {$group:
        {
        _id:"$supplier_id",total:{$sum:"$stock"}
        }
    }
]);
// aggregating Documents using 2 pipeline stages

/*
    Syntax:
        db.collectionName.aggregate([
            {
                $match:{
                    field:value
                }
            },
            {
                $group: 
                {
                    _id:"$supplier_id",total:{$sum: "stocks"}
                }
            }
        ])

*/


     db.fruits.aggregate([
            {
                $match:{
                    field:value
                }
            },
            {
                $group: 
                {
                    _id:"$supplier_id",total:
                    {$sum: "stocks"}
                }
            }
        ])

/*
        "$project" can be used to aggregating data to include/exclude fields from the returned results
*/

db.fruits.aggregate
([
    {
        $project:{_id:0}
    }
])

db.fruits.aggregate([
    {
        $match: {onSale:true},
    },
    {
        $group:{
            _id:"$supplier_id",
            total:{$sum: "$stock"}
               }
    },
    {
        $sort:{total:1}
    }
])

// aggreting results based on the array fields
/*
    "$unwind" deconstructs an array fields from a collection with an arrray value to give us a result for each array elements

    Syntax:
    {$unwind:field}
*/

db.fruits.aggregate([
    {$unwind:"$origin"}
]);

// Display Fruits documents by their origin and the kinds of fruits that are supplied

db.fruits.aggregate
([
    {$unwind:"$origin"},
    {$group:{_id:"$origin", kinds:{$sum:1}}}
])


// pipelining methods

/*
$project show specigic fields
$match match freld w/ some criteria
// match = find
$group grouping the fields to perform 
// 
aggregation
$sort performing the sorting
$limit limit the documents to display
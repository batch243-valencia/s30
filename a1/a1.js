// step 2
db.fruits.aggregate([{ $match: { onSale: true } }, { $count: "onSale" }]);
// step 3
db.fruits.aggregate([
    {
        $match: {stock:{$gt:20}},
    },
    {
        $count:"stocks"
    }
])
// Step 4 -5
db.fruits.aggregate([
    {
        $match: {onSale:true},
    },
    {
         $group:
         {
          _id:"$supplier_id",
             average_price:{$avg:"$price"}
         }
    }    
])

// step 6
db.fruits.aggregate([
    {
         $group:
         {
          _id:"$supplier_id",
             max_price:{$max:"$price"}
         }
    }    
])
// step 7
db.fruits.aggregate([
    {
         $group:
         {
          _id:"$supplier_id",
             min_price:{$min:"$price"}
         }
    }    
])